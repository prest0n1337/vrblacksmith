﻿using UnityEngine;

public class MeshDeformerInput : MonoBehaviour
{
	
	public float force = 10f;
	public float forceOffset = 0.1f;
    public GameObject hammerHead;
	
    void Start()
    {
        hammerHead = GameObject.FindGameObjectWithTag("HammerHead");

    }

    void OnCollisionEnter(Collision collision)
    {
        Debug.Log("Collision");
		Ray inputRay = new Ray(hammerHead.transform.position, hammerHead.transform.right);
		RaycastHit hit;
		
		if (Physics.Raycast(inputRay, out hit))
        {
			MeshDeformer deformer = hit.collider.GetComponent<MeshDeformer>();
			if (deformer)
            {
                Debug.Log(hit.point);
				Vector3 point = hit.point;
				point += hit.normal * forceOffset;
				deformer.AddDeformingForce(point, force);
			}
		}
	}
}